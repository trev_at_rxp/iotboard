 this is the repo for the IOT board
 ¬
 
09 March 2018

Attention:
RXP Services 

Subject:
IoT Board Setup

 
Executive Summary

Background
Instructions to setup IoT touch board for Service Now Forum and RXP Experience centre events.
 
 
Approach
From the front
Cost
Lots and lots. 
Table of Contents
Executive Summary	1
Background	1
Approach	2
Cost	2
Table of Contents	3
1.	Requirements	4
1.1.	Board Connections	4
1.2.	Software	7

Document Controls

Version	Content	Date	Author
1.0			

This document contains confidential and proprietary information that belongs to RXP Services Ltd.  By accepting delivery of this document, you agree to keep its contents confidential.  You also agree not to reproduce its contents, not share its contents with any third party without prior written permission from RXP Services Ltd.


 
1.	Requirements
Raspberry Pi 3
SD Card minimum 8 GB
Coloured LED hats – at least 3
USB to Micro USB connector
Powered USB hub to charge hats
LOLIN32 Lite cards for each hat
500mAh battery for each hat.
Resin.io account
Display board
Bubble machine
8 AA battery holder and batteries
Relay board with 4 relays
Revolting light power supply
Raspberry Pi power supply

2.	Board Connections
The Raspberry Pi is connected to the touch sensor and the relay board. Wiring is based upon the pinout maps below.

 



2.1.	Touch Sensor Connection.


 

2.2.	LOLIN 32







	

Pinout  
•	Arial to 4 – May be blutooth which is not being used.
•	White to GND
•	Red to 27
•	Battery – PKCELL LP503035 3.7v 500mAh
•	Plug in Micro USB to powered hub to charge battery.
2.3.	4 channel Relay
 
This device has the pins soldered on already. The brains are on relay 1, bubbles 2 and light 3.
Relays run from right to left and the left of 3 is red or black and middle the white.

The battery pack supplies power for the brains and the power supply plugs into the circuit to power the flashing light.

3.	Software
3.1.	Raspberry Pi OS
The raspberry Pi 3 runs Resin.io, this requires web access and is uses a web host to monitor the pi and download the required application.
https://resin.io 
user:eddy.rigato@rxpservices.com
pwd:nowforum
Click on the NowForum application
 
Then you see the 2 current devices.
 
Device 1 is the old OS image, lingering-wave is the current SD card.

In the event of a SD failure first delete the old device and then add device.
Select WIFI + ethernet and add wifi settings and download the Resinos install image following the provided steps.

 

Once the Raspberry Pi has booted it will appear in the list and will download the application with the Online green tick appearing.
To just change the WIFI settings place the SD card in a PC and in the resin boot drive that appears, in there you will find a system-connections folder, which contains the NetworkManager config files. The default one is resin-wifi update that file in the relevant sections (the ssid and psk variables)

After booting you may see an error, this requires manually running a command that is in the start.sh file but isn’t working. Click on the device name to see the logs etc.
 

In the lower section select main (not host) and start the terminal session.
Type modprobe i2c-dev and hit enter, this will enable the touch sensor I2C connection.
 
At the top right select restart to restart the app and you are right to go.
 

3.2.	Hats
The hats run Micropython on the ESP32 chip.
You can do all you need from a linux based machine but it is easier to use another Raspberry pi if you can to do this.
Follow the instructions on the site https://www.rototron.info/raspberry-pi-esp32-micropython-tutorial/
It will take you through setting up RSHELL and REPL that are used to connect to the ESP32, this includes how to install the micropython firmware and how to access it

Key files you will need are in the zip file below.
WIFI settings, hat colour and MQTT server address are defined within and easy to find.

 
Once you are connected via rshell to the esp32 copy the main.py file for the hat colour to the /pyboard folder then run REPL and import main.
e.g. for green hat.

Cp main_green.py /pyboard/main.py
Repl
Import main
The hat will boot and connect to wifi and run.
If the hat has been wiped for some reason, install the micropython firmware (the link to download is in the instruction web page) and then 
Mkdir /pyboard/umqtt
Cp uqmtt/robust.py
Cp uqmtt/simple.py
Cp main_green.py /pyboard/main.py
Repl
Import main

And you are running.
Plug in the power for all objects and play
